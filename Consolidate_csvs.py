
import glob
import pandas as pd


path = "./data/clean_data/utf8_encoded/"
allFiles = glob.glob(path + "/*.csv")
frame = pd.DataFrame()
list_ = []
for file_ in allFiles:
    df = pd.read_csv(file_,index_col=None, header=None, names= ['article', 'text_label','numeric_label'])
    list_.append(df)
df_consolidated = pd.concat(list_)

# print(df_consolidated)

df_consolidated.to_csv('./data/clean_data/training_data.csv')