# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 15:27:26 2018

@author: baz10_jqds5yo
"""

from bs4 import BeautifulSoup
from urllib.request import urlopen
import pandas as pd
import re

#list of categories to loop through
categories = ['politics','sport','science']


#########################################################
#Test for one url

search_term = 'science'
url = 'https://www.bbc.co.uk/' + search_term +'?q=science&filter=news&suggid='

#get html from website
html = urlopen(url) 

#create beautiful soup object
soup = BeautifulSoup(html, 'html.parser')

#create dataframe with two columns for output    
df = pd.DataFrame()


#########################################################
#Test for one url

#url = 'https://www.bbc.co.uk/news/uk-england-bristol-45605789'
#html = urlopen(url) 
#soup = BeautifulSoup(html, 'html.parser')
#article = soup.find(class_="story-body__inner")
#article_text = list(article.find_all('p'))
#df = df.append({'article':article_text, 'label':'science'}, ignore_index=True)


#########################################################
#loop through links, append text and label to dataframe    
#need to get everything after: <p class="story-body__introduction">


for link in soup.find_all('a'):    
    url = link.get('href')
    if re.search('.*bbc.co.uk/news.*\d+$',url):
        print('Successful url:')
        print(url)
        html = urlopen(url) 
        soup = BeautifulSoup(html, 'html.parser')
        article = soup.find(class_='story-body__inner')
        article_text = soup.find_all('p')
        #article_text = soup.find_all('p', class_="story-body__introduction")
        #print(article_text)
        #article_text_clean = article_text.rpartition('p class="story-body__introduction"')
        #print(article_text_clean)
        
        df = df.append({'article':article_text, 'label':search_term}, ignore_index=True)
    else:
        print('Failed url:')
        print(url)

print(df)
num_results = len(df)

df.to_csv('google_' + search_term + '_' + str(num_results) + '.csv', index=False)

    