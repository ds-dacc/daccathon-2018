from bs4 import BeautifulSoup
# import beautifulsoup4
import requests
import time
import re
from random import randint
import pandas as pd


def scrape_news_urls(search_term, num_results=0):
    """Function to scrape news urls from google.co.uk.  Returns a list of urls to scrape.
    """
    links = []
    time.sleep(randint(0, 2))  # relax and don’t let google be angry

    #set the url using the search term and number of results
    url = "https://" + f'www.google.co.uk/search?q={search_term}&tbm=nws&start={num_results}'
    r = requests.get(url)
    content = r.text

    #create beautiful soup object
    soup = BeautifulSoup(content, 'html.parser')

    #get the text from all the links
    for link in soup.find_all('a'):
        linktext = link.get('href')
        if '/url?q=' in linktext:
            linktext = linktext.replace('/url?q=', "")
            linktext = linktext.split("&", 1)[0]
            if linktext not in links:
                links.append(linktext)
    return links


def scrape_news_bbc(link):
    """Function to scrape BBC website.  Takes a url and returns the article."""
    r = requests.get(link)
    data = r.text
    soup = BeautifulSoup(data, 'lxml')
    #How to scrape BBC articles
    FullBody = soup.find(class_='story-body__inner')
    article = FullBody.find_all('p')
    #strips out HTML characters
    clean_article =[re.sub(r'<.+?>',r'',str(a)) for a in article]
    # Change list to string
    clean_article = ''.join(clean_article)
    return(clean_article)


def scrape_news_guardian(link):
    """Function to scrape Guardian website.  Takes a url and returns the article."""
    r = requests.get(link)
    data = r.text
    soup = BeautifulSoup(data, 'lxml')
    #How to scrape BBC articles
    article = soup.find_all('p')
    #strips out HTML characters
    clean_article =[re.sub(r'<.+?>',r'',str(a)) for a in article]
    # Change list to string
    clean_article = ''.join(clean_article)
    return(clean_article)

    
def scrape_news_reuters(link):
    """Function to scrape Reuters website.  Takes a url and returns the article."""
    r = requests.get(link)
    data = r.text
    soup = BeautifulSoup(data, 'lxml')
    article_body = soup.find(class_='StandardArticleBody_body')
    article = article_body.find_all('p')
    clean_article = ''.join([p.text for p in p_list])
    return(clean_article)
    
    
def scrape_news_independent(link):
    """Function to scrape Independent website.  Takes a url and returns the article."""
    r = requests.get(link)
    data = r.text
    soup = BeautifulSoup(data, 'lxml')
    article = soup.find_all('p')
    clean_article =[re.sub(r'<.+?>',r'',str(a)) for a in article]
    clean_article = ''.join(clean_article)
    return(clean_article)    
    
    
def add_labels(df, url_col='url', label_col='label'):
    '''
    A function to extract labels from urls for bbc and guardian news articles
    
    Parameters:
        df : a dataframe with a column called url_col
        url_col : the name of the dataframe column containing urls
        label_col : the name of the column that will be appended to the dataframe
    
    Returns the input dataframe with an additional column containing labels
    '''

    df[label_col] = df[url_col].apply(get_label_from_url)
    return df
    
def get_label_from_url(url):
       
    bbc_label_pattern = '(?:.*bbc\.co\.uk/.+/)([a-z-]+)(?:-|/\d+$)'
    guardian_label_pattern = '(?:.*www\.theguardian\.com/)((sport/[a-z-]+)|[a-z-]+)(?:/.*)'
    
    if re.search(bbc_label_pattern, url):
        match = re.search(bbc_label_pattern, url)
        return match.group(1)
    elif re.search(guardian_label_pattern, url):
        match = re.search(guardian_label_pattern, url)
        return match.group(1)
    else:
        return ''



links = []
entries = 100 # How many items you want to scrape
search_term = 'technology' #Enter the search term

for i in list(range(0,entries,10)):
    l = scrape_news_urls(search_term , i)
    links = links + l

articles = []
df = pd.DataFrame()


for link in links:
    #print(link)
    if 'bbc.co.uk/news' in link:
        print(link)
        r = requests.get(link)
        try:
            article = scrape_news_bbc(link)
            df = df.append({'article':article, 'label':search_term, 'url': link}, ignore_index=True)
        except:
            pass

    if 'guardian' in link:
        print(link)
        r = requests.get(link)
        try:
            article = scrape_news_guardian(link)
            df = df.append({'article':article, 'label':search_term, 'url': link}, ignore_index=True)
        except:
            pass

    if 'reuters' in link:
        print(link)
        r = requests.get(link)
        try:
            article = scrape_news_reuters(link)
            df = df.append({'article':article, 'label':search_term, 'url': link}, ignore_index=True)
        except:
            pass           
        
    if 'independent' in link:
        print(link)
        r = requests.get(link)
        try:
            article = scrape_news_independent(link)
            df = df.append({'article':article, 'label':search_term, 'url': link}, ignore_index=True)
        except:
            pass     

print(df)        
num_articles = len(df)
df.to_csv('google_' + search_term + '_' + str(num_articles) + '.csv')  

