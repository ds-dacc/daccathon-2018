"""
This code can be used to consolidate multiple pickle files stored in the ./data directory, into a single dataframe, which
 can then be pickled or converted to a delimited file for export to S3.

It's a little 'unfinished': Manual tweaking is required in two places to append the dataframes into a single dataframe,
based on the number of dataframes that need merging. These parts are called-out in the comments below.
"""


import os
import pandas as pd

#Path to pickle files:
file_path = "./data/"

file_list = os.listdir(file_path)

#Sort list
file_list = sorted(file_list)

# Creates a dictionary of Dataframes, one dataframe per pickle file
# Loop through files and unpickle into DFs
# Code here: https://stackoverflow.com/questions/42977487/how-to-name-dataframes-with-a-for-loop

pickle_dict = {}

for file in file_list:
    df_name = 'df_' + file  # the name for the dataframe
    pickle_dict[df_name] = pd.read_pickle(file_path + str(file))


# Extract dataframes from the dictionary - requires manual intervention based on number of dataframes

df1 = pickle_dict['df_pickle.pkl']
df2 = pickle_dict['df_pickle2.pkl']
# df3 = pickle_dict['df_pickle3.pkl']
# df4 = pickle_dict['df_pickle4.pkl']

# Append dataframes - requires manual intervention based on number of dataframes

df_export = df1.append([df2, df3, df4], ignore_index=True)