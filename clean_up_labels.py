import functools
import csv
import os
import pandas as pd

def label_map_from_csv(csv_file):
    with open(csv_file, 'r') as f:
        reader = csv.reader(f)
        label_map = dict(reader)
    return label_map

def remap_labels(input_df, label_map, label_col='label', append=True, drop=True):
    # Take a copy so that we leave the input df intact
    df = input_df.copy()
    
    # Decide whether to overwrite the label column or append a new clean label column
    if append:
        clean_label_col = 'clean_' + label_col
    else:
        clean_label_col = label_col

    # remap the labels - unidentified labels are set to ''        
    df[clean_label_col] = df[label_col].apply(functools.partial(apply_map, label_map=label_map))
    
    # remove rows where the label is ''
    if drop:
        df = df[df[clean_label_col]!=''].reset_index(drop=True, inplace=False)
    return df
    
def apply_map(label, label_map):    
    if label.lower() in label_map:
        return label_map[label.lower()]
    else:
        return label.lower()


# List the mappings that we want to apply
label_csv_files = [
    '.\data\daily_mail_labels.csv',
    '.\data\Labels.csv'
    ]    
# Create the maps and keep them in a list
map_list = []
for file in label_csv_files:    
    label_map = label_map_from_csv(file)
    map_list.append(label_map)
    
directory = '.\data\daily_mail'
# Open each csv file in the directory as a dataframe and clean the labels
for filename in os.listdir(directory):
    full_filepath = os.path.join(directory, filename)
    clean_filename = filename.replace('.csv', '_clean.csv')
    clean_filepath = os.path.join(directory, clean_filename)
    print(clean_filename)
    if filename.endswith(".csv"):
        df = pd.read_csv(full_filepath, names=['article', 'label'], index_col=False)
        print(full_filepath)
        
    # iterate through the mappings that we want to apply
    label_col = 'label'
    for label_map in map_list:    
        df = remap_labels(df, label_map, label_col=label_col, append=True, drop=True)
        label_col = 'clean_' + label_col
    
    df.to_csv(clean_filepath, columns=['article', 'clean_label', 'clean_clean_label'], header=False, index=False, encoding='utf-8')   