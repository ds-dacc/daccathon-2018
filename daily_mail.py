
from bs4 import BeautifulSoup
import requests
import sys
import pandas as pd


def get_page(url):
    try:
        return requests.get(url).text
    except requests.exceptions.RequestException as e:
        print("Failed to get page " + url)
        print(e)
        sys.exit(1)


def get_parser(text):
    try:
        return BeautifulSoup(text, "html.parser")
    except:
        print("Failed to get parser.")
        sys.exit(1)


def get_article(url):
    try:
        soup = get_parser(get_page(url))
        return soup.find_all("p", class_="mol-para-with-font")
    except:
        pass


def get_article_url(a_tag):
    try:
        return a_tag["href"]
    except:
        pass


def daily_mail():
    try:
        pages = [
            ("Arts-NotFilm",["https://www.dailymail.co.uk/news/arts/index.html"]),
            ("Australia", ["https://www.dailymail.co.uk/auhome/index.html"),
                             "https://www.dailymail.co.uk/news/sydney/index.html",
                             "https://www.dailymail.co.uk/news/melbourne/index.html",
                             "https://www.dailymail.co.uk/news/brisbane/index.html",
                             "https://www.dailymail.co.uk/news/perth/index.html",
                             "https://www.dailymail.co.uk/news/adelaide/index.html"]),
            ("Celebrities", ["https://www.dailymail.co.uk/tvshowbiz/index.html"]),
            ("Health",["https://www.dailymail.co.uk/health/index.html"]),
            ("Science", ["https://www.dailymail.co.uk/sciencetech/index.html"]),
            ("SportFootball",["https://www.dailymail.co.uk/sport/football/index.html"]),
            ("SportNotFootball", ["https://www.dailymail.co.uk/sport/boxing/index.html",
                                  "https://www.dailymail.co.uk/sport/formulaone/index.html",
                                  "https://www.dailymail.co.uk/sport/rugbyunion/index.html",
                                  "https://www.dailymail.co.uk/sport/tennis/index.html",
                                  "https://www.dailymail.co.uk/sport/mma/index.html",
                                  "https://www.dailymail.co.uk/sport/cricket/index.html",
                                  "https://www.dailymail.co.uk/sport/golf/index.html",
                                  "https://www.dailymail.co.uk/sport/racing/index.html"]),
            ("Travel",["https://www.dailymail.co.uk/travel/index.html",
                       "https://www.dailymail.co.uk/travel/destinations/index.html"])
        ]

        for page in pages:
            df = pd.DataFrame()
            filename = page[0] + '_daily_mail_'

            with open(filename + ".txt", "w") as file:
                for pageurl in page[1]:
                    articles = get_parser(get_page(pageurl))

                    for article in articles.find_all("div", class_="articletext"):
                        url = get_article_url(article.find("a"))

                        if url is not None:
                            url = "https://www.dailymail.co.uk" + url
                            lines = get_article(url)
                            text = ""

                            for line in lines:
                                text = text + line.text
                                file.write(line.text)

                            file.write("\n")
                            df = df.append({'article': text,"label": page[0], "url":url},ignore_index=True)

                    df.to_csv(filename + '.csv')

    except:
        print("Program terminated abnormally.")


if __name__ == "__main__":
    daily_mail()
